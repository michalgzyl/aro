package edu.pwr.air.aro.project;

import edu.pwr.air.aro.project.generators.Sequence;


public class SignalClass {
	
	public static int numberOfClasses;
	private int id;
	private double probability;
	private Sequence teachingSeq;
	
	public SignalClass(double probability) {
		numberOfClasses++;
		this.id = numberOfClasses;
		this.probability = probability;
	}
	
	public void teach(Sequence teachingSeq) {
		this.teachingSeq = teachingSeq;
	}
	
	public double getMean() {
		return teachingSeq.getMean();
	}
	
	public double[] getDistancesFromPoint(double point) {
		return teachingSeq.getDistancesFromPoint(point);
	}

	public int getId() {
		return id;
	}

	public double getProbability() {
		return probability;
	}
	
	public Sequence getTeachingSeq()
	{
	    return teachingSeq;
	}
	
	
}

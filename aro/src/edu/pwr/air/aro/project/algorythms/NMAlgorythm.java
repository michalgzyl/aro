package edu.pwr.air.aro.project.algorythms;

import edu.pwr.air.aro.project.SignalClass;
import edu.pwr.air.aro.project.generators.Sequence;

public class NMAlgorythm
    implements RecognitionAlgorythm
{

    SignalClass firstClass;
    SignalClass secondClass;

    @Override
    public int recognizedClass( double newData )
    {
        double distFromFirstClass = Math.abs( firstClass.getMean() - newData );
        double distFromSecondClass = Math.abs( secondClass.getMean() - newData );

        if( distFromFirstClass < distFromSecondClass )
        {
            return firstClass.getId();
        }
        else
        {
            return secondClass.getId();
        }
    }

    @Override
    public void addClasses( SignalClass s1, SignalClass s2 )
    {
        this.firstClass = s1;
        this.secondClass = s2;
    }

    @Override
    public int testAlgorythm( Sequence testSeq, int classId )
    {
        int correctRecognitions = 0;

        for( double signal : testSeq.getData() )
        {
            if( recognizedClass( signal ) == classId )
            {
                correctRecognitions++;
            }
        }

        return correctRecognitions;
    }

}

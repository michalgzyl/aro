package edu.pwr.air.aro.project.algorythms;

import java.util.Arrays;

import edu.pwr.air.aro.project.SignalClass;
import edu.pwr.air.aro.project.generators.Sequence;

public class KNNAlgorythm implements RecognitionAlgorythm {
	
	private int neighboursNumber;
	SignalClass firstClass;
	SignalClass secondClass;
	
	public KNNAlgorythm(int k) {
		this.neighboursNumber = k;
	}

	@Override
	public int recognizedClass(double newData) {
		double[] distFromFirstClass = firstClass.getDistancesFromPoint(newData);
		double[] distFromSecondClass = secondClass.getDistancesFromPoint(newData);
		Arrays.sort(distFromFirstClass);
		Arrays.sort(distFromSecondClass);
		
		int firstCount=0;
		int secondCount=0;
		
		for(int i=0; i<neighboursNumber; i++) {
			if(distFromFirstClass[firstCount] < distFromSecondClass[secondCount]) {
				firstCount++;
			} else {
				secondCount++;
			}
		}
		
		return firstCount > secondCount ? firstClass.getId() : secondClass.getId();
	}

	@Override
	public void addClasses(SignalClass s1, SignalClass s2) {
		this.firstClass = s1;
		this.secondClass = s2;
	}

	@Override
	public int testAlgorythm(Sequence testSeq, int classId) {
		int correctRecognitions = 0;
		
		for(double signal : testSeq.getData()) {
			if(recognizedClass(signal) == classId) {
				correctRecognitions++;
			}
		}
		
		return correctRecognitions;
	}
}

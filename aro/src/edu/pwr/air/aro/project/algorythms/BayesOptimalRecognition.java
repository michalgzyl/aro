package edu.pwr.air.aro.project.algorythms;

import edu.pwr.air.aro.project.SignalClass;
import edu.pwr.air.aro.project.generators.Generator;
import edu.pwr.air.aro.project.generators.Sequence;

public class BayesOptimalRecognition implements RecognitionAlgorythm {

	SignalClass firstClass;
	SignalClass secondClass;
	
	Generator firstInfo;
	Generator secondInfo;
		
	@Override
	public int recognizedClass(double newData) {
		double firstProb = firstInfo.getDensity(newData)*firstClass.getProbability();
		double secondProb = secondInfo.getDensity(newData)*secondClass.getProbability();
		
		if(firstProb > secondProb) {
			return firstClass.getId();
		} else {
			return secondClass.getId();
		}
	}

	@Override
	public int testAlgorythm(Sequence testSeq, int classId) {
		int correctRecognitions = 0;
		
		for(double signal : testSeq.getData()) {
			if(recognizedClass(signal) == classId) {
				correctRecognitions++;
			}
		}
		
		return correctRecognitions;
	}

	@Override
	public void addClasses(SignalClass s1, SignalClass s2) {
		this.firstClass = s1;
		this.secondClass = s2;
	}
	
	public void addProbabilityInfo(Generator g1, Generator g2) {
		firstInfo = g1;
		secondInfo = g2;
	}

}

package edu.pwr.air.aro.project.algorythms;

import edu.pwr.air.aro.project.SignalClass;
import edu.pwr.air.aro.project.generators.Sequence;

public interface RecognitionAlgorythm {
	
	public int recognizedClass(double newData);
	public int testAlgorythm(Sequence testSeq, int classId);
	public void addClasses(SignalClass s1, SignalClass s2);
}

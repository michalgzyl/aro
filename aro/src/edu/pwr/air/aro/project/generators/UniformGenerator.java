package edu.pwr.air.aro.project.generators;


public class UniformGenerator implements Generator {

	private double a;
	private double b;

	public UniformGenerator(double A, double B) {
		this.a = A;
		this.b = B;
	}
	
	@Override
	public double getDensity(double x) {
		if(x>a && x<b) {
			return 1d/(b-a);
		} else {
			return 0;
		}
	}

	@Override
	public double next() {
		return a + rand.nextDouble() * (b - a);
	}

	@Override
	public Sequence getSequence(int numberOfElements) {
		double[] data = new double[numberOfElements];

		for (int i = 0; i < numberOfElements; i++) {
			data[i] = next();
		}

		return new Sequence(data);
	}
}

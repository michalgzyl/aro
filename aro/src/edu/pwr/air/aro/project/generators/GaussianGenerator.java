package edu.pwr.air.aro.project.generators;

public class GaussianGenerator implements Generator {

	private double m;
	private double sigma;

	public GaussianGenerator(double m, double sigma) {
		this.m = m;
		this.sigma = sigma;
	}

	@Override
	public double next() {
		return m + sigma * rand.nextGaussian();
	}

	@Override
	public Sequence getSequence(int numberOfElements) {
		double[] data = new double[numberOfElements];

		for (int i = 0; i < numberOfElements; i++) {
			data[i] = next();
		}

		return new Sequence(data);
	}

	@Override
	public double getDensity(double x) {
		double scaledX = (x - m) / (sigma);
		double denom = Math.sqrt(2 * Math.PI * sigma * sigma);

		return Math.exp(-Math.pow(scaledX, 2) / 2) / denom;
	}
}

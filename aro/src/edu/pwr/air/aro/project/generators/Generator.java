package edu.pwr.air.aro.project.generators;

import java.util.Random;

public interface Generator {
	public Random rand = new Random();
	
	public double getDensity(double x);
	public double next();
	public Sequence getSequence(int numberOfElements);	
}

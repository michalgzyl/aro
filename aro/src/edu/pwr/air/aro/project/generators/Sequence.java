package edu.pwr.air.aro.project.generators;

public class Sequence {
	
	private double[] data;
	private int size;
	
	public Sequence(double[] data) {
		this.data = data;
		this.size = data.length;
	}
	
	public double getMean() {
		double mean=0;
		
		for(double d : data) {
			mean+=d;
		}
		
		return mean/size;
	}
	
	public double[] getDistancesFromPoint(double point) {
		double[] result = new double[size];
		
		for(int i=0; i<size; i++) {
			result[i] = Math.abs(data[i] - point);
		}
		
		return result;
	}

	public double[] getData() {
		return data;
	}

	public int getSize() {
		return size;
	}
}

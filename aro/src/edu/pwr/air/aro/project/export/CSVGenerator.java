package edu.pwr.air.aro.project.export;

import java.io.FileWriter;
import java.io.IOException;

/**
 * @author Paweł Chrząszczewski
 *
 */
public class CSVGenerator
{
    private static CSVGenerator instance = null;

    protected CSVGenerator()
    {
    }

    public static CSVGenerator getInstance()
    {
        if( instance == null )
        {
            instance = new CSVGenerator();
        }
        return instance;
    }

    public void GenerateCsvFile( String fileName, int id, double[] data )
    {
        try
        {
            FileWriter writer = new FileWriter( fileName );

            for( double d : data )
            {
                writer.append( String.valueOf( id ) );
                writer.append( ',' );
                writer.append( String.valueOf( d ) );
                writer.append( '\n' );
            }

            writer.flush();
            writer.close();
        }
        catch( IOException e )
        {
            e.printStackTrace();
        }
    }
}

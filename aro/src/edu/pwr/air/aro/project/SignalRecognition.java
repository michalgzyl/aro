package edu.pwr.air.aro.project;

import edu.pwr.air.aro.project.algorythms.BayesOptimalRecognition;
import edu.pwr.air.aro.project.algorythms.KNNAlgorythm;
import edu.pwr.air.aro.project.algorythms.NMAlgorythm;
import edu.pwr.air.aro.project.algorythms.RecognitionAlgorythm;
import edu.pwr.air.aro.project.generators.GaussianGenerator;
import edu.pwr.air.aro.project.generators.UniformGenerator;
import edu.pwr.air.aro.project.generators.Generator;
import edu.pwr.air.aro.project.generators.Sequence;

public class SignalRecognition
{

    public static void main( String[] args )
    {
        double average = 0;
        
        //prawdopodobienstwa apriori wystapienia klas 
        double firstClassProb = 0.7;
        double secondClassProb = 1 - firstClassProb;
        
        Generator firstDist = new GaussianGenerator( 0, 4 );
        Generator secondDist = new GaussianGenerator( 4, 4 );

        for( int i = 1; i <= 10; i++ ) // zwiekszamy ciag uczacy
        {      	
            Sequence teachingSeq1 = firstDist.getSequence( (int)(firstClassProb * 10 * i) );
            Sequence teachingSeq2 = secondDist.getSequence( (int)(secondClassProb * 10 * i) );

            SignalClass firstClass = new SignalClass( firstClassProb );
            SignalClass secondClass = new SignalClass( secondClassProb );

            firstClass.teach( teachingSeq1 );
            secondClass.teach( teachingSeq2 );

            // CSVGenerator.getInstance().GenerateCsvFile(
            // "firstClass.csv", firstClass.getId(), firstClass.getTeachingSeq().getData() );

//            RecognitionAlgorythm nm = new KNNAlgorythm(1);
//            RecognitionAlgorythm nm = new NMAlgorythm();
            BayesOptimalRecognition nm = new BayesOptimalRecognition();
            nm.addProbabilityInfo(firstDist, secondDist);
            
            nm.addClasses( firstClass, secondClass );
            
            for( int j = 1; j <= 100; j++ ) // wykonujemy dla 100 obrazów
            {            	
                Sequence testSeq1 = firstDist.getSequence( (int)(firstClassProb * 50) );
                Sequence testSeq2 = secondDist.getSequence( (int)(secondClassProb * 50) );
    
                double correct = (double)(nm.testAlgorythm( testSeq1, firstClass.getId() ) +
                		nm.testAlgorythm( testSeq2, secondClass.getId() )) / 50.0;
                average += correct;
            }
            average = average / 100;
            average = 1 - average;
            System.out.println( String.format( "%6s,   %.6f", 10*i, average ) );
            average = 0;
        }
    }
}

